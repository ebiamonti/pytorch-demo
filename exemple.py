#!/usr/bin/env python3

import os
import torch
from torch import nn
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision.transforms import ToTensor

model_dir = './models/'
model_name = 'model'

epochs = 7

# Download training data from open datasets.
training_data = datasets.FashionMNIST(
    root="data",
    train=True,
    download=True,
    transform=ToTensor(),
)

# Download test data from open datasets.
test_data = datasets.FashionMNIST(
    root="data",
    train=False,
    download=True,
    transform=ToTensor(),
)

batch_size = 64

# Create data loaders.
train_dataloader = DataLoader(training_data, batch_size=batch_size)
test_dataloader = DataLoader(test_data, batch_size=batch_size)

for X, y in test_dataloader:
    print(f"Shape of X [N, C, H, W]: {X.shape}")
    print(f"Shape of y: {y.shape} {y.dtype}")
    break

# Get cpu or gpu device for training.
device = "cuda" if torch.cuda.is_available() else "cpu"
print(f"Using {device} device")

# Define model
class NeuralNetwork(nn.Module):
    def __init__(self):
        super(NeuralNetwork, self).__init__()
        self.flatten = nn.Flatten()
        self.linear_relu_stack = nn.Sequential(
            nn.Linear(28*28, 512),
            nn.ReLU(),
            nn.Linear(512, 512),
            nn.ReLU(),
            nn.Linear(512, 10)
        )

    def forward(self, x):
        x = self.flatten(x)
        logits = self.linear_relu_stack(x)
        return logits

model = NeuralNetwork().to(device)

#loading exisitng model if exisits
if os.path.exists(model_dir + model_name + '.pth'):
    print("Using model " + model_dir + model_name + '.pth')
    model.load_state_dict(torch.load(model_dir + model_name + '.pth'))

print(model)

#optimization
loss_fn = nn.CrossEntropyLoss()
optimizer = torch.optim.SGD(model.parameters(), lr=1e-3)

def train(dataloader, model, loss_fn, optimizer):
    size = len(dataloader.dataset)
    model.train()
    for batch, (X, y) in enumerate(dataloader):
        X, y = X.to(device), y.to(device)

        # Compute prediction error
        pred = model(X)
        loss = loss_fn(pred, y)

        # Backpropagation
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        if batch % 100 == 0:
            loss, current = loss.item(), batch * len(X)
            print(f"loss: {loss:>7f}  [{current:>5d}/{size:>5d}]")

def test(dataloader, model, loss_fn):
    size = len(dataloader.dataset)
    num_batches = len(dataloader)
    model.eval()
    test_loss, correct = 0, 0
    with torch.no_grad():
        for X, y in dataloader:
            X, y = X.to(device), y.to(device)
            pred = model(X)
            test_loss += loss_fn(pred, y).item()
            correct += (pred.argmax(1) == y).type(torch.float).sum().item()
    test_loss /= num_batches
    correct /= size
    print(f"Test Error: \n Accuracy: {(100*correct):>0.1f}%, Avg loss: {test_loss:>8f} \n")


import sys

def save_model():
    torch.save(model.state_dict(), model_dir + model_name + '.pth')
    print("Saved PyTorch Model State to model.pth")


def train_epochs(save = False):
    for t in range(epochs):
        print(f"Epoch {t+1}\n-------------------------------")
        train(train_dataloader, model, loss_fn, optimizer)
        test(test_dataloader, model, loss_fn)
        print("Done!")
    if (save):
        save_model()

def handle_arg_img():
    print('Handeling custom image...')
    
    from PIL import Image
    import PIL.ImageOps
    import torchvision.transforms as transforms
    img = Image.open(sys.argv[1]).convert('L').resize((28, 28))
    img = PIL.ImageOps.invert(img)
    #img.save('image_tesst.jpg')
    transform = transforms.Compose([
        transforms.ToTensor()
    ])
    test_data2 = transform(img)
    return test_data2

def predict(classes, custom, test_data=test_data):
    if custom :
        x = test_data
        y = 1
    else :
        x = test_data[1][0]
        y = test_data[1][1]

    with torch.no_grad():
        pred = model(x)
        print(type(pred))
        print(pred)
        predicted, actual = classes[pred[0].argmax(0)], classes[y]
        print(f'Predicted: "{predicted}", Actual: "{actual}"')

def main() -> int:
    #try:
    #train_epochs(True)

    classes = [
            "T-shirt/top",
            "Trouser",
            "Pullover",
            "Dress",
            "Coat",
            "Sandal",
            "Shirt",
            "Sneaker",
            "Bag",
            "Ankle boot",
        ]

    model.eval()
    if (len(sys.argv) > 1):
        print('------ 1')
        print(test_data[0][0])
        test_data2 = handle_arg_img()
        print('------ 2')
        print(test_data2)
        predict(classes, True, test_data2)
    else :
        predict(classes, False)
    return 0
    #except:
     #   return 1

if __name__ == '__main__':
    sys.exit(main())  # next section explains the use of sys.exit
