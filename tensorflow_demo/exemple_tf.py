#!/usr/bin/env python3

import torch

# sample execution (requires torchvision)
from PIL import Image
from torchvision import transforms

import argparse
import time

#meta-tensorflow/recipes-model/googlecodelabs/tensorflow-for-poets/label_image_lite.py
import cv2

captured_img_pth = 'data/captured_image.png'

def take_picture():
  cap = cv2.VideoCapture(0) # video capture source camera (Here webcam of laptop) 
  ret,frame = cap.read() # return a single frame in variable `frame`

  while(True):
    cv2.imshow('img1',frame) #display the captured image
    if cv2.waitKey(1) & 0xFF == ord('y'): #save on pressing 'y' 
      cv2.imwrite(captured_img_pth, frame)
      cv2.destroyAllWindows()
      break

  cap.release()

def load_labels(filename):
  my_labels = []
  input_file = open(filename, 'r')
  for l in input_file:
    my_labels.append(l.strip())
  return my_labels

'''model_file = "./optimized_graph.lite"'''
def label_image(image = None,
                model_file = "./files/mobilenet_v2.zip",
                label_file = "./files/imagenet_labels.txt",
                input_mean = 127.5,
                input_std = 127.5,
                floating_model = False):
    if image is None:
        return None, None

    labels = load_labels(label_file)
    #model = torch.hub.load('pytorch/vision:v0.10.0', 'mobilenet_v2', pretrained=True)
    model = torch.load(model_file)
    
    input_image = Image.open(image)
    # create a mini-batch as expected by the model
    preprocess = transforms.Compose([
      transforms.Resize(256),
      transforms.CenterCrop(224),
      transforms.ToTensor(),
      transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
    ])
    input_tensor = preprocess(input_image)
    input_batch = input_tensor.unsqueeze(0)
    
    start_time = time.time()
    
    model.eval()
    # move the input and model to GPU for speed if available 
    if torch.cuda.is_available():
      input_batch = input_batch.to('cuda')
      model.to('cuda')
    with torch.no_grad():                                                                  
      output = model(input_batch)
    end_time = time.time()
    print("--- in %s seconds ---" % round((end_time - start_time), 5))
    

    # Tensor of shape 1000, with confidence scores over Imagenet's 1000 classes
    #print(output[0])
    
    # The output has unnormalized scores. To get probabilities, you can run a softmax on it.
    probabilities = torch.nn.functional.softmax(output[0], dim=0)
    #print(probabilities)
    
    # Show 5 top categories for image
    top5_prob, top5_catid = torch.topk(probabilities, 5)
    print("Top 5 probabilities for " + image)
    for i in range(top5_prob.size(0)):
      print(labels[top5_catid[i]], top5_prob[i].item())
    torch.save(model, './mobilenet_v2')


if __name__ == "__main__":
  floating_model = False

  parser = argparse.ArgumentParser()
  parser.add_argument("-i", "--image", default="./data/granny-smith.jpg", \
    help="image to be classified")
  parser.add_argument("-m", "--model_file", \
    default="./files/mobilenet_v2.zip", \
    help=".tflite model to be executed")
  parser.add_argument("-l", "--label_file", default="./files/imagenet_labels.txt", \
    help="name of file containing labels")
  parser.add_argument("--input_mean", default=127.5, help="input_mean")
  parser.add_argument("--input_std", default=127.5, \
    help="input standard deviation")
  parser.add_argument("-c", "--capture", default=False, \
    help="capture image with camera")
  args = parser.parse_args()

  if args.capture:
    take_picture()
    args.image = captured_img_pth

  label_image(args.image, args.model_file, args.label_file,
              args.input_mean, args.input_std, floating_model)
  label_image(args.image, args.model_file, args.label_file,
              args.input_mean, args.input_std, floating_model)
  label_image(args.image, args.model_file, args.label_file,
              args.input_mean, args.input_std, floating_model)
  label_image("./data/dog.jpg", args.model_file, args.label_file,
              args.input_mean, args.input_std, floating_model)
  label_image("./data/dog.jpg", args.model_file, args.label_file,
              args.input_mean, args.input_std, floating_model)
  label_image("./data/mushroom.jpg", args.model_file, args.label_file,
              args.input_mean, args.input_std, floating_model)
